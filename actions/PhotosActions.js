export const ADD_PHOTO = 'ADD_PHOTO';
export const GET_API_PHOTOS = 'GET_API_PHOTOS';
export const REMOVE_PHOTO = 'REMOVE_PHOTO';
export const SET_MODAL_PICTURE = 'SET_MODAL_PICTURE';

export function add(content) {
	if (!content) {
		return;
	}

	return {
		type: ADD_PHOTO,
		payload: content,
	};
}

export function setModalPicture(content) {
	if (!content) {
		return;
	}

	return {
		type: SET_MODAL_PICTURE,
		payload: content,
	};
}

export function getApiPhotos(content) {
	if (!content) {
		return;
	}

	return {
		type: GET_API_PHOTOS,
		payload: content,
	};
}

export function remove(index) {
	return {
		type: REMOVE_PHOTO,
		payload: index,
	};
}