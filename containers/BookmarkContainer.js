import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { remove } from './../actions/PhotosActions';
import Bookmark from './../page/Bookmark';

const linkStateToComponent = (state, props) => ({
	bookmarkPhotos: state.photos.bookmarkPhotos
})
const linkActionsToComponent = dispatch => ({
	remove: bindActionCreators(remove, dispatch)
})
export default connect(linkStateToComponent, linkActionsToComponent)(Bookmark);