import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { add, remove} from './../actions/PhotosActions';
import FavButton from './../components/FavButton';

const linkStateToComponent = (state, props) => ({
	bookmarkPhotos: state.photos.bookmarkPhotos,
	ApiPhotos: state.photos.ApiPhotos
})
const linkActionsToComponent = dispatch => ({
	remove: bindActionCreators(remove, dispatch),
	add: bindActionCreators(add, dispatch)
})

export default connect(linkStateToComponent, linkActionsToComponent)(FavButton);