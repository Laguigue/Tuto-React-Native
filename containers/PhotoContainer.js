import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { add, remove, setModalPicture} from './../actions/PhotosActions';
import Photo from './../components/Photo';

const linkStateToComponent = (state, props) => ({
	bookmarkPhotos: state.photos.bookmarkPhotos,
	ApiPhotos: state.photos.ApiPhotos,
	ModalPicture: state.photos.modalPicture
})
const linkActionsToComponent = dispatch => ({
	remove: bindActionCreators(remove, dispatch),
	add: bindActionCreators(add, dispatch),
	setModalPicture: bindActionCreators(setModalPicture, dispatch)
})

export default connect(linkStateToComponent, linkActionsToComponent)(Photo);