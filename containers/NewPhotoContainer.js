import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { add, getApiPhotos } from './../actions/PhotosActions';
import News from './../page/News';

const linkStateToComponent = (state, props) => ({
	ApiPhotos: state.photos.ApiPhotos
})
const linkActionsToComponent = dispatch => ({
	add: bindActionCreators(add, dispatch),
	getApiPhotos: bindActionCreators(getApiPhotos, dispatch)
})
export default connect(linkStateToComponent, linkActionsToComponent)(News);