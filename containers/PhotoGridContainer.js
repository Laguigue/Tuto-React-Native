import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setModalPicture} from './../actions/PhotosActions';
import PhotosGrid from './../components/PhotosGrid';

const linkStateToComponent = (state, props) => ({
	ModalPicture: state.photos.modalPicture
})
const linkActionsToComponent = dispatch => ({
	setModalPicture: bindActionCreators(setModalPicture, dispatch)
})

export default connect(linkStateToComponent, linkActionsToComponent)(PhotosGrid);