import { ADD_PHOTO, GET_API_PHOTOS, REMOVE_PHOTO, SET_MODAL_PICTURE } from './../actions/PhotosActions';

const initialState = {
	bookmarkPhotos: [],
	ApiPhotos: [],
	modalPicture: {}
};

export default function photoState(state = initialState, action) {
	switch (action.type) {
		case ADD_PHOTO:
			return {
				...state,
				bookmarkPhotos: [
					...state.bookmarkPhotos,
					action.payload
				]
			}
		case SET_MODAL_PICTURE:
			return {
				...state,
				modalPicture: action.payload
			}
		case GET_API_PHOTOS:
				return {
					...state,
					ApiPhotos: action.payload
				}
		case REMOVE_PHOTO:
			var updatedBookmarks = state.bookmarkPhotos.filter(photo => photo.id !== action.payload);

			return {
				...state,
				bookmarkPhotos: updatedBookmarks
			}
		default:
			return state;
	}
}