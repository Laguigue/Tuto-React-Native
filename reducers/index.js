import { combineReducers } from 'redux';
import navState from './navigatorReducer';
import photoState from './bookmarkReducer';

const state = combineReducers({
	nav: navState,
	photos: photoState
});

export default state;