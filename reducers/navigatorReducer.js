import { NavigationActions } from 'react-navigation';
import { RootNavigator } from '../RootNavigator';

export default function navState(state, action) {
	const nextState = RootNavigator.router.getStateForAction(action, state);
	return nextState || state;
}