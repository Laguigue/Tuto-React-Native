import React, {Component} from 'react';
import Unsplash from 'unsplash-js/native';
import PhotosGrid from './../containers/PhotoGridContainer';
import { View, Text, TouchableHighlight, Alert, FlatList, Image, StyleSheet } from 'react-native';

const unsplash = new Unsplash({
	applicationId: "9efd091a45f2c735010d9aa47832acf5ef044a9f30728732ca9d8707b7476fb5",
	secret: "4bfd0567f3abff4da6a850d1e26588978f4d15329abda5cec6757ffe62529783",
	callbackUrl: "urn:ietf:wg:oauth:2.0:oob"
});

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
	}
});

class SampleScreen extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		if (this.props.ApiPhotos.length === 0) {
			unsplash.photos.listPhotos(2, 8, "latest")
				.then(rep => {
					return rep.json();
				})
				.then(photos => {
					this.props.getApiPhotos(photos)
				});
		}
	}

	render() {
		if (this.props.ApiPhotos.length > 0) {
			return (
				<View style={styles.container}>
						<PhotosGrid
							photos={this.props.ApiPhotos}
						>
						</PhotosGrid>
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					{
						<Text>Loading</Text>
					}
				</View>
			)
		}
	}
}

export default SampleScreen;