import React, {Component} from 'react';
import PhotosGrid from './../containers/PhotoGridContainer';
import { View, Text, StyleSheet, PhotoGrid, FlatList, Image, TouchableHighlight } from 'react-native';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
	}
});

class Bookmark extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		if (this.props.bookmarkPhotos.length > 0) {
			return(
				<View style={styles.container}>
					<PhotosGrid
						photos={this.props.bookmarkPhotos}
					>
					</PhotosGrid>
				</View>
			)
		} else {
			return(
				<View style={styles.container}>
					<Text>No items stored yet!</Text>
				</View>
			)
		}
	}
}

export default Bookmark;