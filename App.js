import React from 'react';
import { Provider } from 'react-redux';
import RootNavigator from './RootNavigator';
import store from './store';

const AppProvider = () => (
  <Provider store={store}>
    <RootNavigator />
  </Provider>
);

export default AppProvider;
4