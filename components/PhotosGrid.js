import React, { Component } from 'react';
import { View, Text, Modal, StyleSheet, FlatList, TouchableHighlight, TouchableOpacity, Image } from 'react-native';
import Photo from './../containers/PhotoContainer';

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	list: {
		paddingTop: 18,
		padding: 2.5
	},
	photoBig: {
		flex: 1,
	},
	photoLoader: {
		width: 50,
		height: 50
	},
	toolBar: {
		display: 'flex',
		flexDirection: 'row',
		height: 50,
		zIndex: 1000,
		marginTop: 18,
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	closeButton: {
		textAlign: 'center'
	},
	toolBarItem: {
		flex: 1,
		justifyContent: 'center'
	},
	loaderContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0
	}
});

class PhotosGrid extends Component {
	constructor(props) {
		super(props);
	}

	closeModal() {
		this.props.setModalPicture({})
	}

	render () {
		console.log(this.props.ModalPicture)
		return (
			<View style={styles.container}>
				<Modal
					animationType="slide"
					transparent={false}
					visible={Object.keys(this.props.ModalPicture).length > 0}
				>
					<View style={styles.toolBar}>
						<TouchableOpacity
							onPress={() => this.closeModal({})}
							style={styles.toolBarItem}
						>
							<Text style={styles.closeButton}>X</Text>
						</TouchableOpacity>
						<Text>{}</Text>
					</View>

					<View style={styles.loaderContainer}>
						<Image
							style={styles.photoLoader}
							source={{uri: 'https://loading.io/spinners/heart/lg.beating-heart-preloader.gif'}}
						/>
					</View>
					<Image
						style={styles.photoBig}
						source={{uri: Object.keys(this.props.ModalPicture).length > 0 ? this.props.ModalPicture.urls.raw : ''}}
					/>
				</Modal>

				<FlatList
					data={this.props.photos}
					numColumns={2}
					style={styles.list}
					renderItem={({item}) =>
					<Photo item={item}/>
				}
				/>
			</View>
		)
	}
}

export default PhotosGrid;