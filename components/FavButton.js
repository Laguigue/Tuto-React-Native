import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

const styles = StyleSheet.create({
	// block_image: {
	// 	flex: 0.5,
	// 	margin: 5
	// },
	icon: {
		flex: 1,
		paddingTop: 8,
		paddingBottom: 8,
		textAlign: 'center',
		color: 'pink'
	}
});

class FavButton extends Component {
	constructor(props) {
		super(props);
		this.onPress = this._onPressButton.bind(this);
	}

	_onPressButton (item) {
		console.log('coucou', item)
		if (!this._isFavorited(item)) {
			this.props.add(item)
		} else {
			this.props.remove(item.id)
		}
	}

	_isFavorited (item) {
		let isItemInBookmark = this.props.bookmarkPhotos.find(photo => photo.id === item.id)
		if (typeof isItemInBookmark === 'undefined') {
			return false
		}
		return true
	}

	render () {
		return (
			<TouchableOpacity
				onPress={() => this._onPressButton(this.props.item)}
				style={styles.block_image}
			>
				<View>
					{this._isFavorited(this.props.item) ? <FontAwesome style={styles.icon}>{Icons.heart}</FontAwesome> : <FontAwesome style={styles.icon}>{Icons.heartO}</FontAwesome>}
				</View>
			</TouchableOpacity>
		)
	}
}

export default FavButton;