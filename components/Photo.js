import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableHighlight, Image } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import FavButton from './../containers/FavButtonContainer';

const styles = StyleSheet.create({
	block_image: {
		flex: 0.5,
		margin: 5
	},
	image: {
		flex: 1,
		height: 200
	}
});

class Photo extends Component {
	constructor(props) {
		super(props);
		this.onPress = this._onPressButton.bind(this);
	}

	_onPressButton () {
		this.props.setModalPicture(this.props.item)
	}

	_isFavorited (item) {
		let isItemInBookmark = this.props.bookmarkPhotos.find(photo => photo.id === item.id)
		if (typeof isItemInBookmark === 'undefined') {
			return false
		}
		return true
	}

	render () {
		return (
			<TouchableHighlight
				onPress={() => this._onPressButton()}
				style={styles.block_image}
			>
				<View>
					<Image
						source={{uri: this.props.item.urls.thumb}}
						style={styles.image}
					/>
					<FavButton item={this.props.item}></FavButton>
				</View>
			</TouchableHighlight>
		)
	}
}

export default Photo;