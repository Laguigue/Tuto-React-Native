import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, TabNavigator } from 'react-navigation';
import News from './containers/NewPhotoContainer';
import Bookmark from './containers/BookmarkContainer';
import Search from './page/Search';

const tabScenes = {
	News: { screen: News },
	Bookmark: { screen: Bookmark },
	ScreenC: { screen: Search }
};

const tabOptions = {
	initialRouteName: 'News',
	swipeEnabled: true,
	animationEnabled: true,
	tabBarOptions: {
		activeTintColor: '#fff',
		activeBackgroundColor: 'rgba(0,0,0,0.8)',
		labelStyle: {
			fontSize: 16,
		},
	},
};

export const RootNavigator = TabNavigator(tabScenes, tabOptions);

const TabRouter = ({ dispatch, nav }) => (
	<RootNavigator
		navigation={addNavigationHelpers({
      dispatch,
      state: nav
    })} />
);

TabRouter.propTypes = {
	dispatch: PropTypes.func.isRequired,
	nav: PropTypes.object.isRequired,
};

export default connect(state => ({
	nav: state.nav
}))(TabRouter);